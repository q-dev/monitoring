## Make it possible to obtain data
First you need to run a small docker-compose file on the instance where you are going to get the information. The contents of this file are given below
```
version: '3'

services:
  node-exporter:
    image: prom/node-exporter:v1.3.0
    volumes:
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /:/rootfs:ro
    command: 
      - '--path.procfs=/host/proc' 
      - '--path.sysfs=/host/sys'
      - --collector.filesystem.ignored-mount-points
      - "^/(sys|proc|dev|host|etc|rootfs/var/lib/docker/containers|rootfs/var/lib/docker/overlay2|rootfs/run/docker/netns|rootfs/var/lib/docker/aufs)($$|/)"
    ports:
      - 8200:9100
    restart: unless-stopped
```

## Add your nodes to monitoring
You need no edit the prometheus/prometheus.yml file by adding your instance info, you could add as many nodes as you want

## Change grafana admin password
You can also change the Grafana administrator password to make your changes to the dashboard in grafana/config.monitoring


After all of this you should just run 
```
docker-compose up -d
``` 
And your grafana dashboard will be avalible on YOUR_MACHINE_IP:3000
